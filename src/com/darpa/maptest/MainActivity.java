package com.darpa.maptest;

import java.util.List;

import android.app.Activity;
import android.location.Location;
import android.os.Bundle;
import android.os.RemoteException;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

import com.darpa.map.MapDataServiceReporter;
import com.darpa.map.MapServiceManager;
import com.darpa.map.PointCloud;
import com.google.android.gms.maps.model.LatLng;


public class MainActivity extends Activity {
	
	private MapServiceManager mapManager;
	TextView textView;
	
	 @Override
	    public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        requestWindowFeature(Window.FEATURE_PROGRESS);
	        setContentView(R.layout.activity_main);
	        
	        textView = (TextView)findViewById(R.id.textView);
	        
	    }
    
	    @Override
	    protected void onStart() { 
	    	Log.d("Main", "onStart");
	    	mapManager = new MapServiceManager(this, new MapServiceManager.OnConnectedListener() {
	    		@Override public void onConnected() {
	    			Log.d("MainActivity2", "connected - adding reporter");
	    			mapManager.add(reporter);
	    		}
	    		@Override public void onDisconnected() {
	    			Log.d("MainActivity2", "disconnected - removing reporter");
	    			mapManager.remove(reporter);
	    		}
	    	});
	    	
	    	super.onStart();
	    }
	    

	    @Override
	    protected void onStop() {
	    	mapManager.disconnect();
	    	mapManager = null;
	    	super.onStop();
	    }
	    
	    private MapDataServiceReporter reporter = new MapDataServiceReporter.Stub() {
			@Override
			public void reportMap(PointCloud pointCloud) throws RemoteException {
				if(pointCloud!=null && textView!=null){
					Log.i("Map","in report loop: "+pointCloud.getPointCloud().size());
					textView.setText("Map size: "+pointCloud.getPointCloud().size());
					//textView.setText(map.toArray().toString());
				
				}}};
	
}


			
	    


