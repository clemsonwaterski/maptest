package com.darpa.map;

import com.darpa.map.MapDataService;
import com.darpa.map.MapDataServiceReporter;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class MapServiceManager {
	private boolean disconnected = false;
    private MapDataService mapDataService;
    private static Intent intent = new Intent("com.darpa.map.MapDataService");
    private OnConnectedListener onConnectedListener;
    private Activity activity;

    public static interface OnConnectedListener {
    	void onConnected();
    	void onDisconnected();
    }

	public MapServiceManager(Activity activity, OnConnectedListener onConnectedListener) {
    	this.activity = activity;
		this.onConnectedListener = onConnectedListener;
		activity.bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
	}
	
	public synchronized void disconnect() {
		disconnected = true;
		activity.unbindService(serviceConnection);
	}
	public synchronized void add(MapDataServiceReporter reporter) {
		if (disconnected)
			throw new IllegalStateException("Manager has been explicitly disconnected; you cannot call methods on it");
		try {
			mapDataService.add(reporter);
		} catch (RemoteException e) {
			Log.e("MainActivity", "add reporter", e);
		}
	}
	public synchronized void remove(MapDataServiceReporter reporter) {
		if (disconnected)
			throw new IllegalStateException("Manager has been explicitly disconnected; you cannot call methods on it");
		try {
			mapDataService.remove(reporter);
		} catch (RemoteException e) {
			Log.e("MainActivity", "remove reporter", e);
		}
	}

    private ServiceConnection serviceConnection = new ServiceConnection() {
		@Override public void onServiceDisconnected(ComponentName name) {
			Log.d("MSM", "onServiceDisconnected");
			if (onConnectedListener != null)
				onConnectedListener.onDisconnected();
			mapDataService = null;
		}
		@Override public void onServiceConnected(ComponentName name, IBinder service) {
			mapDataService = MapDataService.Stub.asInterface(service);
			if (onConnectedListener != null)
				onConnectedListener.onConnected();
		}
	};
}
