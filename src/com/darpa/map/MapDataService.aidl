package com.darpa.map;

import com.darpa.map.MapDataServiceReporter;

interface MapDataService {
	void add(MapDataServiceReporter reporter);
	void remove(MapDataServiceReporter reporter);
}