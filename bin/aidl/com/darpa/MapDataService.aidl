package com.darpa;

import com.darpa.MapDataServiceReporter;

interface MapDataService {
	void add(MapDataServiceReporter reporter);
	void remove(MapDataServiceReporter reporter);
}