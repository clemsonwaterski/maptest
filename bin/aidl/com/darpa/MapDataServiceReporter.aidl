package com.darpa;

import com.google.android.gms.maps.model.LatLng;

interface MapDataServiceReporter {
	void reportMap(in List<LatLng> map);
}